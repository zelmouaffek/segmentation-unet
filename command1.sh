#!/bin/bash

horovodrun -np 1 python main.py --data_dir /data --model_dir /results  --exec_mode train    --max_steps 40000 
horovodrun -np 1 python main.py --data_dir /data --model_dir /results  --exec_mode predict
