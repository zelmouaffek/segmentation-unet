#!/bin/bash
#SBATCH --gres=gpu:1
#SBATCH --mem=50g
set -x
export NV_GPU=$SLURM_JOB_GPUS
#INSTANCIER UN CONTAINER DOCKER COMME DANS EXEMPLE CI DESSOUS
nvidia-docker run --rm --name $DNAME -v /raid/IRMA/zelmouaffek/UNet_Medical/:/raid/IRMA/zelmouaffek/UNet_Medical -v /raid/IRMA/zelmouaffek/UNet_Medical/data_denoised_contrast:/data -v /raid/IRMA/zelmouaffek/UNet_Medical/results_denoised_contrast:/results -w /raid/IRMA/zelmouaffek/UNet_Medical unet_tf:latest /raid/IRMA/zelmouaffek/UNet_Medical/command.sh